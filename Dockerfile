FROM ubuntu:14.04

EXPOSE 80

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y pandoc python-pip

RUN pip install webapp2 webob paste jinja2 requests

COPY . /opt/pandocticon
WORKDIR /opt/pandocticon

ENTRYPOINT ["python", "pandocticon.py"]