# Pandocticon

Web UI for converting html to mediawiki syntax by URL or text input.

Currently tailored for use by W3C SocialWG for meeting minutes.

## Todo

* Give all to/from syntax options

## Run your own

* Install docker
* In pandocticon root: `docker build -t "rhiaro/pandocticon" . ` then `docker run -d rhiaro/pandocticon`.
* Or with jwilder/nginxproy `sudo docker run -d -e VIRTUAL_HOST=pandoc.domain.tld -v /path/to/pandocticon:/opt/pandocticon --name pandoc rhiaro/pandocticon`