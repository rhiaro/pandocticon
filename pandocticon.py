import os
import requests
import datetime
import webapp2
import pypandoc
import jinja2
from paste.cascade import Cascade
from paste.urlparser import StaticURLParser

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), 
autoescape=True)

def html2mw(html_in):
    print type(html_in)
    output = pypandoc.convert(html_in, 'mediawiki', format='html')
    return output

def raw_minutes_url():
    today = datetime.date.today()
    return today.strftime("http://www.w3.org/%Y/%m/%d-social-minutes.html")

def wiki_page_url():
    today = datetime.date.today()
    return today.strftime("https://www.w3.org/wiki/Socialwg/%Y-%m-%d-minutes")

class Index(webapp2.RequestHandler):
    def get(self):
        template = jinja_env.get_template('index.html')
        self.response.content_type = 'text/html'
        self.response.out.write( template.render({'url': raw_minutes_url(), 'wikiurl': wiki_page_url()}) )

    def post(self):
        html = self.request.get('in')
        url = self.request.get('url')
        if not html: 
            r = requests.get(url)
            html = r.text
        
        mw = html2mw(html)
        template = jinja_env.get_template('index.html')
        self.response.content_type = 'text/html'
        self.response.out.write( template.render({'out':mw, 'in':html, 'url': url, 'wikiurl': wiki_page_url()}) )

application = webapp2.WSGIApplication([
    ('/', Index),
], debug=True)

static = StaticURLParser("./")
app = Cascade([application, static])

def main():
    from paste import httpserver
    httpserver.serve(app, host='0.0.0.0', port='80')

if __name__ == '__main__':
    main()